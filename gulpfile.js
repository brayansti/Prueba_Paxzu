var gulp = require('gulp');
var webserver = require('gulp-webserver');
var less = require('gulp-less');
var path = require('path');
var jade = require('gulp-jade');


//  conf less

gulp.task('less', function () {
  return gulp.src('Html/css/main.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    // destino
    .pipe(gulp.dest('Html/css/'));
});

//  conf web server
gulp.task('server', function() {
  gulp.src('')
    .pipe(webserver({
      //host: '0.0.0.0',
      port: 8080,
      livereload: true
    }));
});

// conf jade

gulp.task('templates', function() {
	var YOUR_LOCALS = {};

	gulp.src('Html/*.jade')
	.pipe(jade({
	locals: YOUR_LOCALS,
        pretty: true
	}))
	// destino
	.pipe(gulp.dest('Html/'))
});


// wacth

gulp.task('watch', function() {
  gulp.watch('Html/css/main.less',['less']);
  gulp.watch('Html/*.jade',['templates']);
})

// tareas default
gulp.task('default', ['less','watch','server','templates']);
